//
//  SceneDelegate.h
//  LaunchPage
//
//  Created by Bee on 2020/11/12.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

